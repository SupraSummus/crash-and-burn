#!/bin/sh

# make tmp file
tmp=`mktemp`

# compile
echo "
#include <stdio.h>
int main(){
	printf(\"hello there, looser\n\");
	__asm__(\""$@"\");
	printf(\"that's all\n\");
	return 0;
};
" | gcc -xc - -o $tmp

# run
exec $tmp
