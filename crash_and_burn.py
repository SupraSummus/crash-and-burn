#!/bin/python

from subprocess import call
from tempfile import mktemp
from os import remove
import argparse

### parse args ###

parser = argparse.ArgumentParser(description='Executes assembly code, then prints registers.')
parser.add_argument(
	'registers',
	metavar='register',
	type=str,
	nargs='*',
	help='register to use with optional initial value `name[=initial]`'
)
parser.add_argument(
	'code',
	metavar='code',
	type=str,
	nargs=1,
	help='assembly code to be executed'
)

args = parser.parse_args()

registers = []
for arg in args.registers:
	register = {}
	if arg.find("=") == -1:
		register["name"] = arg
		register["initial"] = None
	else:
		register["name"] = arg[:arg.find("=")]
		register["initial"] = arg[arg.find("=") + 1:]
	registers.append(register)

store = ""
for register in registers:
	store += "push " + register["name"] + "; "

restore = ""
for register in reversed(registers):
	restore += "pop " + register["name"] + "; "

init = ""
for register in registers:
	if register["initial"] != None:
		init += "mov " + register["initial"] + ", " + register["name"] + "; "

### make tmp files ###

source_file = mktemp(suffix='.c')
binary_file = mktemp()

### generate source ###

open(source_file, "w").write("""
#include <stdio.h>
int main(){
	printf("hello there, looser\\n");
	__asm__(" """ + store + """ ");
	__asm__(" """ + init + """ ");
	__asm__(" """ + args.code[0] + """ ");
	__asm__(" """ + restore + """ ");
	printf("that's all\\n");
	return 0;
};
""")

### compile, run, clean ###

if call(["gcc", "-o", binary_file, source_file]) == 0:
	call([binary_file,])
	remove(binary_file)

remove(source_file)

